package ru.t1.semikolenov.tm.service;

import ru.t1.semikolenov.tm.api.repository.IProjectRepository;
import ru.t1.semikolenov.tm.api.service.IProjectService;
import ru.t1.semikolenov.tm.enumerated.Sort;
import ru.t1.semikolenov.tm.enumerated.Status;
import ru.t1.semikolenov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.semikolenov.tm.exception.field.*;
import ru.t1.semikolenov.tm.model.Project;
import ru.t1.semikolenov.tm.model.Task;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public List<Project> findAll(final Comparator comparator) {
        if (comparator == null) return findAll();
        return projectRepository.findAll(comparator);
    }

    @Override
    public List<Project> findAll(final Sort sort) {
        if (sort == null) return findAll();
        return findAll(sort.getComparator());
    }

    @Override
    public Project remove(final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.remove(project);
        return project;
    }

    @Override
    public Project create(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return projectRepository.create(name);
    }

    @Override
    public Project create(final String name, String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return projectRepository.create(name, description);
    }

    @Override
    public Project create(String name, String description, Date dateBegin, Date dateEnd) {
        final Project project = create(name, description);
        if (project == null) throw new ProjectNotFoundException();
        if (dateBegin == null) throw new DateBeginIncorrectException();
        else project.setDateBegin(dateBegin);
        if (dateEnd == null) throw new DateEndIncorrectException();
        else project.setDateEnd(dateEnd);
        return project;
    }

    @Override
    public Project add(final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        return projectRepository.add(project);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }


    @Override
    public Project findOneById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return projectRepository.findOneById(id);
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= projectRepository.getSize()) throw new IndexIncorrectException();
        return projectRepository.findOneByIndex(index);
    }

    @Override
    public Project updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateById(final String id, final String name, final String description, Date dateBegin, Date dateEnd) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        if (dateBegin == null) throw new DateBeginIncorrectException();
        else project.setDateBegin(dateBegin);
        if (dateEnd == null) throw new DateEndIncorrectException();
        else project.setDateEnd(dateEnd);
        return project;
    }

    @Override
    public Project updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= projectRepository.getSize()) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(final Integer index, final String name, final String description, Date dateBegin, Date dateEnd) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= projectRepository.getSize()) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        if (dateBegin == null) throw new DateBeginIncorrectException();
        else project.setDateBegin(dateBegin);
        if (dateEnd == null) throw new DateEndIncorrectException();
        else project.setDateEnd(dateEnd);
        return project;
    }

    @Override
    public Project removeById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return projectRepository.removeById(id);
    }

    @Override
    public Project removeByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= projectRepository.getSize()) throw new IndexIncorrectException();
        return projectRepository.removeByIndex(index);
    }

    @Override
    public Project changeProjectStatusById(String id, Status status) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(Integer index, Status status) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= projectRepository.getSize()) throw new IndexIncorrectException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }
}
