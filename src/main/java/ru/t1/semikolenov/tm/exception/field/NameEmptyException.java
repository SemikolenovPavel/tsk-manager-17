package ru.t1.semikolenov.tm.exception.field;

public class NameEmptyException extends AbstractFieldException {

    public NameEmptyException() {
        super("Error! Name is empty...");
    }

}
