package ru.t1.semikolenov.tm.api.service;

import ru.t1.semikolenov.tm.model.Task;

public interface IProjectTaskService {

    Task bindTaskToProject(String projectId, String taskId);

    Task unbindTaskFromProject(String projectId, String taskId);

    void removeProjectById(String projectId);

}
