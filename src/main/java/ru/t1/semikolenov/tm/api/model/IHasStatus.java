package ru.t1.semikolenov.tm.api.model;

import ru.t1.semikolenov.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
